https://app.diagrams.net<?php
require_once('../db/DBManager.php');
use DBManager;

function getImage(){

    $manager = new DBManager();
    try{
        $sql = 'SELECT imagen FROM cliente WHERE id=3';
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result[0]['imagen'];
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function updateCliente($image){

    $manager = new DBManager();
    try{
        error_log("init update");
        $sql = 'UPDATE cliente SET imagen=:img WHERE id=3';
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindValue(':img',$image,PDO::PARAM_LOB);
        $stmt->execute();
        error_log("fin update");
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function insertCliente($cliente){

    $manager = new DBManager();
    try{
        $sql="INSERT INTO cliente (nombre, apellidos, fecha_nacimiento, sexo, telefono, dni, email, password) VALUES (:nombre,:apellidos,:fecha_nacimiento,:sexo,:telefono,:dni,:email,:password)";
        $nombre=$cliente->getNombre();
        $apellidos=$cliente->getApellidos();
        $fecha_nacimiento=$cliente->getFechaNacimiento();
        $sexo=$cliente->getSexo();
        $telefono=$cliente->getTelefono();
        $dni=$cliente->getDni();
        $email=$cliente->getEmail();
        $password=password_hash($cliente->getPassword(),PASSWORD_DEFAULT,['cost'=>10]);

        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':nombre',$nombre);
        $stmt->bindParam(':apellidos',$apellidos);
        $stmt->bindParam(':fecha_nacimiento',$fecha_nacimiento);
        $stmt->bindParam(':sexo',$sexo);
        $stmt->bindParam(':telefono',$telefono);
        $stmt->bindParam(':dni',$dni);
        $stmt->bindParam(':email',$email);
        $stmt->bindParam(':password',$password);

        if ($stmt->execute()){
            echo "todo OK";
        }else{
            echo "MAL";
        }
        if ( password_verify('123456','$2y$10$xA/vyZ8Yn8hmpPyHnLwNe.GfZxj8bc.ZchHW6PwL9EzFb0AW0wUYS') ){
            echo 'Iguales<br/>';
        }else{
            echo 'Diferentes<br/>';
        }


    }catch (PDOException $e){
        echo $e->getMessage();
    }


}

?>
?>
