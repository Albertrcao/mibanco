<form action="controller.php" method="post">
    <select name="cuentas">

        <?php
        require_once('model/CuentaModel.php');
        $accounts=getAccounts('dni');
        for ($i=0; $i<sizeof($accounts) ;$i++){?>
            <option ><?php echo $accounts[$i]["cuenta"] ?></option>
        <?php }?>
    </select>
    <input name="submit" type="submit" value="Seleccionar"/>
    <input name="control" type="hidden" value="query"/>
</form>


<?php
session_start();
if (isset($_SESSION['saldo']))
    echo $_SESSION['saldo'];
session_start();
if (isset($_SESSION['saldo'])) {
    echo "Saldo " . $_SESSION['saldo'] . '<br/>';
}
if (isset($_SESSION['lista'])) {
    $movimientos=$_SESSION['lista'];
    echo '<table class="default" rules="all" frame="border">';
    echo '<tr>';
    echo '<th>origen</th>';
    echo '<th>destino</th>';
    echo '<th>hora</th>';
    echo '<th>cantidad</th>';
    echo '</tr>';
    for ($i=0;$i<count($movimientos);$i++){
        echo '<tr>';
        echo '<td>'.$movimientos[$i]['origen'].'</td>';
        echo '<td>'.$movimientos[$i]['destino'].'</td>';
        echo '<td>'.$movimientos[$i]['hora'].'</td>';
        echo '<td>'.$movimientos[$i]['cantidad'].'</td>';
        echo '</tr>';
    }
    echo '</table>';

}

if($_POST['control']=='query') {
    session_start();
    $_SESSION['saldo']=getSaldo($_POST['cuentas']);
    $_SESSION['lista']=getMovimientos($_POST['cuentas']);
    header("Location: query.php");
}

function getMovimientos($cuenta)
{

    $manager = new DBManejador();
    try {
        $sql = "SELECT * FROM movimientos WHERE origen=:cuenta";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':cuenta', $cuenta);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $manager->cerrarConexion();
        return $rt;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

if($_POST['control']=='select_account'){
    $saldo = getSaldo($_POST['cuentas']);
    session_start();
    $_SESSION['saldo']=$saldo;
    $_SESSION['lista']=getMovimientos($_POST['cuentas']);
    header("Location: query.php");

}
?>
