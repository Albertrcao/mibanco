<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo _("Registrar-se");?></title>
</head>
<body>
<form action="../Controller/controller.php" method="post">
    <label for="fname">Nom: </label><br>
    <input type="text" id="fname" placeholder="Introdueix el teu nom" pattern="[A-Za-z]{3,40}" name="fname"><br>
    <label for="sname">Cognoms: </label><br>
    <input type="text" id="sname" placeholder="Introdueix els teus cognoms" pattern="[A-Za-z ]{3,50}" name="sname"><br>
    <label for="sexe">De quin sexe ets?</label><br>
    <select name="sexe" id="sexe">
        <option value="home">Home</option>
        <option value="dona">Dona</option>
        <option value="nop">Prefereixo no dir-ho</option>
    </select><br>
    <label for="años">Data de naixement:</label><br>
    <input type="date" id="años" name="trip-años" value="30-10-2020" min="1900-01-01" max="30-10-2020"><br>
    <label for="dni">DNI:</label><br>
    <input type="text" name="dni" placeholder="Introdueix el seu DNI" pattern="[0-9]{8}[A-Za-z]{1}" title="Has de posar 8 números i una lletra"><br>
    <label for="tel">Número de telèfon:</label><br>
    <input type="tel" placeholder="Introdueix el teu número de telèfon" name="tel" pattern="[6-7]{1}[0-9]{8}"><br>
    <label for="mail">Email:</label><br>
    <input type="email" placeholder="Introdueix el teu email" name="mail" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1-5}"><br>
    <label for="contrasenya">Contrasenya:</label><br>
    <input type="password" name="contrasenya" pattern="[A-Za-z0-9!?-]{8-21}"><br>
    <input name="control" value="register" type="hidden"><br>
    <input name="submit" value="Registrar-se" type="submit">
</form>
</body>
</html>
