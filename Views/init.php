<?php
require_once('../helpers/i18n.php');
echo _("Bienvenido a tu banco de confianza =)");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome to MiBanco</title>
</head>
<body>
<?php require_once('header.php');?>
<form action="../Controller/controller.php" method="POST">
    <h3>Que desea?</h3>

    <a href="register.php"> <input name="reg" type="button" value="Crear cuenta"></a>
    <input name="control" type="hidden" value="login"/>
    <br/><br/>

    <a href="login.php"><input name="ins" type="button" value="Iniciar Sesión"></a>
    <input name="control" type="hidden" value="login"/>
</form>


<!-- Mostrar cuentas -->
<form action="../Controller/controller.php" method="post">
    <select name="cuentas">

        <?php
        require_once('model/cuentaModel.php');
        $accounts = getAccounts('dni');
        for ($i=0; $i<sizeof($accounts) ;$i++){?>
            <option ><?php echo $accounts[$i]["cuenta"] ?></option>
        <?php }?>
    </select>
    <input name="submit" type="submit" value="Seleccionar"/>
    <input name="control" type="hidden" value="select_account"/>
</form

</body>
</html>
