<?php


function validateRegister(){

    if (!validateName()){
        $_POST['errors'].='Solamente se acepta carácteres <br/>';
    }
    if (!validateSurName()){
        $_POST['errors'].='Solamente se acepta carácteres <br/>';
    }
    if (!validateDNI()){
        $_POST['errors'].='Tiene que ser 8 numeros y una letra mayuscula <br/>';
    }
    if (!validateTel()){
        $_POST['errors'].='Te pedimos 9 números, tiene que empezar por un 6 o un 7 <br/>';
    }
    if (!validateEmail()){
        $_POST['errors'].='Esta escrita incorrectamente, compruebalo <br/>';
    }
    if (!validateContrasenya()){
        $_POST['errors'].='Contraseña incorrectamente escrita <br/>';
    }
}




function validateName(){
    if (preg_match_all("[A-Za-z]{3,40}", $_POST['fname'])){
        return true;
    }
    return false;
}
function validateSurName(){
    if (preg_match_all("[A-Za-z ]{3,50}", $_POST['sname'])){
        return true;
    }
    return false;
}
function validateDNI(){
    if (preg_match_all('/[0-9]{8}[A-Z]{1}/', $_POST['dni'])){
        return true;
    }
    return false;
}
function validateTel(){
    if (preg_match_all('/[6-7]{1}[0-9]{8}/', $_POST['tel'])){
        return true;
    }
    return false;
}
function validateEmail(){
    if (preg_match_all('/[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1-5}/')){
        return true;
    }
    return false;
}
function validateContrasenya(){
    if (preg_match_all('[A-Za-z0-9!?-]{8-21}')){
        return true;
    }
    return false;
}
?>
